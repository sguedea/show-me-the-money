
# INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here.

	
# PROJECT SPECIFICATIONS
* Fetch random users from the randomuser.me API
* Use forEach() to loop and output user/wealth
* Use map() to double wealth
* Use filter() to filter only millionaires
* Use sort() to sort by wealth
* Use reduce() to add all wealth 
