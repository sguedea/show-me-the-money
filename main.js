
var data = [];

// Buttons' Functionality
// 1. target the buttons by their id names and attach a click event.
// 2. pass it the proper function names as the click method's parameter.
// HINT: Use jQuery
// addUserBtn.addEventListener('click', getRandomUser);
// doubleBtn.addEventListener('click', doubleMoney);
// sortBtn.addEventListener('click', sortByRichest);
// showMillionairesBtn.addEventListener('click', showMillionaires);
// calculateWealthBtn.addEventListener('click', calculateWealth);
$("#add-user").click(getRandomUser);
$("#double").click(doubleMoney);
$("#sort").click(sortByRichest);
$("#show-millionaires").click(showMillionaires);
$("#calculate-wealth").click(calculateTotalWealth);


// gets a random user from an api - completed
// HINT: This is how you'll get a user on your webpage
function getRandomUser() {
    $.ajax("https://randomuser.me/api")
        .done(function (response) {
            console.log(response.results);

            const user = response.results[0];

            const newUser = {
                name: `${user.name.first} ${user.name.last}`,
                money: Math.floor(Math.random() * 1100000)
            };

            addData(newUser);
        });
}


// double the money - this should return the user's name, and the user's money by multiplied by 2
function doubleMoney() {
    data = data.map(user => {
        return {name: user.name, money: user.money * 2}
    });
    updateDOM();
}


// sort by richest - this should sort the users based on their money value highest to lowest
function sortByRichest() {
    data.sort((a, b) => b.money - a.money);
    updateDOM();
}


// filter millionaires - this should only show the user that has their money value greater than 1000000
function showMillionaires() {
    data = data.filter(user => user.money > 1000000);
    updateDOM();
}


// cal total wealth - this should add all of the users money together
// and be displayed
function calculateTotalWealth() {

    const wealth = data.reduce((acc, user) => (acc += user.money), 0);
    const wealthEL = document.createElement('div');
    wealthEL.innerHTML += `<h3>Total Wealth: ${formatMoney(wealth)}</h3>`;
    $("#main").append(wealthEL);
}


// add new object to data arr - completed
function addData(obj) {
    data.push(obj);

    updateDOM();
}


// update DOM - completed - but can be updated to be displayed differently
function updateDOM(providedData = data) {
    // clear main div
    $("#main").html(`<h2><strong>Person</strong> Wealth</h2>`);

    providedData.forEach(item => {
        const element = document.createElement("div");

        element.classList.add('person');
        element.innerHTML = `<strong> ${item.name}</strong> ${formatMoney(item.money)}`;
        $("#main").append(element);
    });
}


// format number as money - reformat the output of the amount
// https://stackoverflow.com/questions/149055/how-to-format-numbers-as-currency-string
function formatMoney(number) {
    return '$' + number
}



